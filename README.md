# Screenshot wall

Deze clientside webpagina toont alle interactives in het museum op een pagina.
Zoeken kan op naam. Klikken op het plaatje levert een lightbox met een grotere
versie. De afbeeldingen worden iedere 20 seconden opnieuw gehaald. Deze
applicatie werkt alleen binnen het VPN van het museum.

## Updaten

Code aanpassen in Repo, na push gaat CI/CD nieuwe container aanmaken, daarna op de server

```cd /opt/dir/naar/project```

daarna  

```
docker-compose pull
docker-compose up -d
```
